# API Evoucher.ggg.com.vn for the new TGS
Tương ứng với mỗi Promotion trên TGS, sẽ được gắn với mã chương trình marketing. TGS server gọi vào hệ thống evoucher.ggg.com.vn để sinh mã code


## Voucher Object
- **id**: _string_ id của voucher trên hệ thống evoucher
- **code**: _string_ mã voucher 
- **value**: _float_ giá trị voucher
- **currency**: _string_ loại tiền tệ của voucher, mặc định VND
- **isRedeemed**: _bool_ true - khi voucher đã đc sử dụng; false - ngược lại
- **createTime**: _datetime_ thời gian tạo voucher, e.g: '2017-12-31 20:13:14', 
- **redeemTime**: _datetime_ thời gian sử dụng, e.g: '2017-12-31 20:13:14',
- **expiredTime**: _datetime_ hạn sử dụng e.g: '2018-03-31 20:13:14',
- **customer**: _customer object_ thông tin người dùng
- **campaign**: _campaign object_ thông tin campaign marketing
 
## Customer bbject
- **email**: _string_ email khách hàng
- **cellphone**: _string_ điện thoại,
- **fullname**: _string_ họ tên
## Campaign object
- **code**: _string_ mã chương trình marketing,
- **name**: _string_ tên chương trình marketing

## 1. Generate Voucher

### Method `GET`

### URI `/voucher/generate`

### Sending params
- **email**: _string_ email khách hàng
- **campaign**: _string_ mã chương trình marketing
- **cellphone**: _string_ (option) điện thoại khách hàng
- **fullname**: _string_ (option) họ tên khách hàng

### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: voucher, loại resource đc trả về
- **result** _object_: voucher object, object voucher đc api trả về

#### Example
- curl https://api.../voucher/generate?email=toan.nd@ggg.com.vn&campaign=ggcs \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'voucher',
        result: {
            id: 123
            code: 'ggcs123abc'
            value: 50000
            currency: 'VND'
            isRedeemed: false,
            createTime: '2017-12-31 20:13:14', 
            redeemTime:  null,
            expiredTime: '2018-03-31 20:13:14',
            customer: {
                email: 'toan.nguyenduc@ggg.com.vn',
                cellphone: null,
                fullname: 'Nguyễn Đức Toàn'
            },
            campaign: {
                code: 'ggcs',
                name: 'Gogi Ăn 4 tặng 1 tháng 02/2018'
            }
        }
    }
```

## 2. Get voucher information

### Method `GET`

### URI `/voucher/code/{code}`

### Sending params
none

### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: voucher, loại resource đc trả về
- **result** _object_: voucher object, object voucher đc api trả về

### Example
- curl https://api.../voucher/code/ggcs123abc \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'voucher',
        result: {
            id: 123
            code: '123abc'
            value: 50000
            currency: 'VND'
            isRedeemed: true,
            createTime: '2017-12-31 20:13:14', 
            redeemTime:  '2018-05-10 20:13:14',
            expiredTime: '2018-03-31 20:13:14',
            customer: {
                email: 'toan.nguyenduc@ggg.com.vn',
                cellphone: null,
                fullname: 'Nguyễn Đức Toàn'
            },
            campaign: {
                code: 'ggcs',
                name: 'Gogi Ăn 4 tặng 1 tháng 02/2018'
            }
        }
    }
```

## 3. Get customer's voucher

### Method `GET`

### Sending params
- **campaign**: _string_ (option) mã chương trình marketing

### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: voucher, loại resource đc trả về
- **result** _object_: array voucher object, object voucher đc api trả về

### Example
- curl https://api.../voucher/customer/toan.nguyenduc@ggg.com.vn&campaign=ggcs \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'voucher',
        result: [
            {
                id: 123
                code: 'ggcs123abc'
                value: 50000
                currency: 'VND'
                isRedeemed: true,
                createTime: '2017-12-31 20:13:14', 
                redeemTime:  '2018-05-10 20:13:14',
                expiredTime: '2018-03-31 20:13:14',
                customer: {
                    email: 'toan.nguyenduc@ggg.com.vn',
                    cellphone: null,
                    fullname: 'Nguyễn Đức Toàn'
                },
                campaign: {
                    code: 'ggcs',
                    name: 'Gogi Ăn 4 tặng 1 tháng 02/2018'
                }
            },
            {
                id: 345
                code: 'ggcs123abc'
                value: 50000
                currency: 'VND'
                isRedeemed: false,
                createTime: '2017-12-31 20:13:14', 
                redeemTime: null,
                expiredTime: '2018-03-31 20:13:14',
                customer: {
                    email: 'toan.nguyenduc@ggg.com.vn',
                    cellphone: null,
                    fullname: 'Nguyễn Đức Toàn'
                },
                campaign: {
                    code: 'ggcs',
                    name: 'Gogi Ăn 4 tặng 1 tháng 02/2018'
                }
            }
        ]    
    }
```
