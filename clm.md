# List CLM API FOR THE NEW TGS

## I. Customer

### 1. Get customer info
#### Method `GET` 

#### URI `/customer\id|email|cellphone/{id|email|cellphone}`

#### Sending params
none

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: customer, loại resource đc trả về
- **result** _object_: customer object, object khách hàng đc api trả về 

#### Example
- `GET` `/customer/cellphone/0987802175`
- `GET` `/customer/email/toan.nguyenduc@ggg.com.vn`
- `GET` `/customer/id/123321`

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'customer',
        result: {
            id: 123456,
            firstname: 'Toàn',
            lastname: 'Nguyễn',
            middlename: 'Đức',
            fullname: 'Nguyễn Đức Toàn',
            cellphone: '0987802175',
            email: 'toan.nguyenduc@ggg.com.vn',
            address: '315 Trường Chinh, Đống Đa, Hà Nội',
            birthday: '2018-12-31',
            gender: 'Male',
            class: 'Gold',
            balance: [
                {
                    name: 'G-Coin',
                    total: 15000,
                    pendingAmount: 5000,
                    availableAmount: 10000
                },
                {
                    name: 'G-Point',
                    total: 1500000,
                    pendingAmount: 500000,
                    availableAmount: 1000000
                },
            ]
        }
    }
```

### 2. Update customer info
#### Method `PUT`

#### URI `/customer/{id}`

#### Sending params
###### If these params are present, update them. Empty string is not null.
- **email** _string_: (option) email
- **cellphone** _string_: (option) số điện thoại
- **firstname** _string_: (option) tên
- **lastname** _string_: (option) họ
- **middlename** _string_: (option) tên đệm
- **fullname** _string_: (option) họ tên đầy đủ
- **address** _string_: (option) địa chỉ
- **birthday** _datetime_: (option) ngày sinh, e.g: 2018-13-31
- **gender** _string_: (option) giới tính, e.g: Male/Female/Unknown

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: customer, loại resource đc trả về
- **result** _object_: customer object, object khách hàng đc api trả về

#### Example
curl https://api..../customer/123456 \ 
  -X PUT
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'
  -d '{"firstname": "Toàn", "lastname" : "Nguyễn"}'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'customer',
        result: {
            id: 123456,
            firstname: 'Toàn',
            lastname: 'Nguyễn',
            middlename: 'Đức',
            fullname: 'Nguyễn Đức Toàn',
            cellphone: '0987802175',
            email: 'toan.nguyenduc@ggg.com.vn',
            address: '315 Trường Chinh, Đống Đa, Hà Nội',
            birthday: '2018-12-31',
            gender: 'Male',
            class: 'Gold'
        }
    }
```

### 3. Create Customer
#### Method `POST`

#### URI `/customer/`

#### Sending params
###### Need pass at least one of email or cellphone or both of them.

- **email** _string_: email
- **cellphone** _string_: số điện thoại
- **firstname** _string_: (option) tên
- **lastname** _string_: (option) họ
- **middlename** _string_: (option) tên đệm
- **fullname** _string_: (option) họ tên đầy đủ
- **address** _string_: (option) địa chỉ
- **birthday** _datetime_: (option) ngày sinh, e.g: 2018-13-31
- **gender** _string_: (option) giới tính, e.g: Male/Female/Unknown

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: customer, loại resource đc trả về
- **result** _object_: customer object, object khách hàng đc api trả về

#### Example
curl https://api.../customer \ 
  -X POST
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'
  -d '{"cellphone": "0987802175"}'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'customer',
        result: {
            id: 123456,
            firstname: null,
            lastname: null,
            middlename: null,
            fullname: null,
            cellphone: '0987802175',
            email: null,
            address: null,
            birthday: null,
            gender: null,
            class: null
        }
    }
```

## II. Voucher
### 1. Get customer's voucher
#### Method `GET`

#### URI `/voucher/customer/{customerId}`

#### Sending params
- **page** _int_: (option) trang
- **numberPerPage** _int_: (option) số lượng mỗi trang

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: voucher, loại resource đc trả về
- **result** _array_: array voucher object

#### Example
- curl https://api.../customer?page=2&numberPerPage=20 \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'voucher',
        result: [
            {
                id: 123
                code: '123abc'
                value: 50000
                currency: 'VND'
                isRedeemed: false,
                createTime: '2017-12-31 20:13:14', 
                redeemTime:  null,
                expiredTime: '2018-03-31 20:13:14'
            },
            {
                id: 124
                code: '123abcbda'
                value: 50000
                currency: 'VND'
                isRedeemed: false,
                createTime: '2017-12-31 20:13:14', 
                redeemTime:  null,
                expiredTime: '2018-04-30 20:13:14'
            },
            {
                id: 121
                code: '1235423'
                value: 50000
                currency: 'VND'
                isRedeemed: true,
                createTime: '2017-12-31 20:13:14', 
                redeemTime: '2018-01-20 20:13:14',
                expiredTime: '2017-14-31 20:13:14'
            } 

        ]
    }
```

### 2. Get voucher
#### Method `GET`

#### URI `/voucher/code/{code}`

#### Sending params
- **page** _int_: (option) trang
- **numberPerPage** _int_: (option) số lượng mỗi trang

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: voucher, loại resource đc trả về
- **result** _object_: array voucher object

#### Example
- curl https://api.../voucher/code/123abc \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'voucher',
        result: {
            id: 123
            code: '123abc'
            value: 50000
            currency: 'VND'
            isRedeemed: false,
            createTime: '2017-12-31 20:13:14', 
            redeemTime: null,
            expiredTime: '2018-12-31 20:13:14',
        }
    }
```

## III. Transaction
### 1. Get customer's transaction

#### Method `GET`

#### URI `transaction/customer/{customerId}`

#### Sending params
- **page** _int_: (option) trang
- **numberPerPage** _int_: (option) số lượng mỗi trang
- **fromDate** _string_: (option) từ ngày, e.g: 2017-12-31
- **toPage** _int_: (option) đến ngày, e.g: 2018-12-31

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: transaction, loại resource đc trả về
- **result** _array_: array transaction object

#### Example
- curl https://api.../transaction/customer/123456?page=2&numberPerPage=20&fromDate=2017-12-31&toDate=2018-12-31 \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'voucher',
        result: [
            {
                id: 123312
                restaurantCode: 4
                restaurantName: 'Ashima Triệu Việt Vương'
                time: '2018-04-20 20:15:24',
                customerId: 345654,
                subTotal: 1500000,
                discountTotal: 0,
                vatAmount: 150000,
                totalBillValue: 1650000,
                walletGain: 75000,
                totalVoucherValueAmount: 300000
            },
            {
                id: 123312
                restaurantCode: 4
                restaurantName: 'Ashima Triệu Việt Vương'
                time: '2018-04-20 20:15:24',
                customerId: 345654,
                subTotal: 1500000,
                discountTotal: 0,
                vatAmount: 150000,
                totalBillValue: 1650000,
                walletGain: 75000,
                totalVoucherValueAmount: 300000
            },
        ]
    }
```

### 2. Get a transaction

#### Method `GET`

#### URI `transaction/id/{id}`

#### Sending params
none

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: transaction, loại resource đc trả về
- **result** _object_: transaction object

#### Example
- curl https://api.../transaction/id/123312 \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'transaction',
        result: {
            id: 123312
            restaurantCode: 4
            restaurantName: 'Ashima Triệu Việt Vương'
            time: '2018-04-20 20:15:24',
            customerId: 345654,
            subTotal: 1500000,
            discountTotal: 0,
            vatAmount: 150000,
            totalBillValue: 1650000,
            walletGain: 75000,
            totalVoucherValueAmount: 300000
       }
    }
```

## IV. Temporary Transaction
### 1. Get Temporary transaction

#### Method `GET`

#### URI `temporary/qrCode/{qrCode}`

#### Sending Params
none

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: transaction, loại resource đc trả về
- **result** _object_: temporary transaction object

#### Example
- curl https://api.../temporaryTransaction/qrCode/123312 \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'
  
- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: 'temporary transaction',
        result: {
            id: 123312
            restaurantCode: 4
            restaurantName: 'Ashima Triệu Việt Vương'
            time: '2018-04-20 20:15:24',
            customerId: 345654,
            subTotal: 1500000,
            discountTotal: 0,
            vatAmount: 150000,
            totalBillValue: 1650000,
            limitVoucherValueAmount: 750000
       }
    }
```

### 2. Cancel Temporary transaction

#### Method `PUT`

#### URI `temporary/qrCode/{qrCode}`

#### Sending Params
none

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: none
- **result** _object_: none

#### Example
- curl https://api.../temporaryTransaction/qrCode/123312 \
  -X 'PUT'
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'
  
- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: null,
        result: null
    }
```

### 3. Confirm Temporary transaction

#### Method `POST`

#### URI `temporary/qrCode/{qrCode}`

#### Sending Params
- **voucher** _string_: json danh sách mã voucher sử dụng thanh toán

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: transaction
- **result** _object_: transaction object

#### Example
- curl https://api.../temporaryTransaction/qrCode/123312 \
  -X 'PUT'
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'
  -H '{"voucher" : [{"code": "1234512"},{"code": "23423523"}]}'
  
- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: transaction,
        result: {
            id: 123312
            restaurantCode: 4
            restaurantName: 'Ashima Triệu Việt Vương'
            time: '2018-04-20 20:15:24',
            customerId: 345654,
            subTotal: 1500000,
            discountTotal: 0,
            vatAmount: 150000,
            totalBillValue: 1650000,
            walletGain: 75000,
            totalVoucherValueAmount: 300000
        }
    }
```

## V. Restaurant
### 1. Get list restaurant

#### Method `GET`

#### URI `restaurant\list`

#### Sending Params
- **region** _string_: (option) Ha Noi|Ho Chi Minh|Vinh
- **brand** _string_: (option) Kichi Kichi|Gogi House|....
- **concept** _string_: (option) Hotspot|Beer|Western|....

#### Response
- **messageCode** _int_: mã api trả về, 1 thành công, khác 1 là lỗi.
- **message** _string_: api message, mô tả lỗi khi gọi api
- **resource** _string_: restaurant
- **result** _array_: array restaurant object

#### Example
- curl https://api.../restaurant/list?region=Ha Noi&concept=Hotspot \
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'
  
- Response
```
    {
        messageCode: 1,
        message: 'success',
        resource: restaurant,
        result: [
            {
                restaurantCode: 4,
                restaurantName: "Ashima Triệu Việt Vương",
                regionName: "Ha Noi",
                brandName: "Ashima",
                latitude: 10.123141,
                longitude: 11.21314,
                thumbnail: "https://....ggg.com.vn/path-to-ashima-trieu-viet-vuong-thumbnail.jpg",
                address: "Số 123 Triệu Việt Vương, Ba Đình, Hà Nội",
                avgRating: 4.56
            },
            {
                restaurantCode: 4,
                restaurantName: "Ashima Triệu Việt Vương",
                regionName: "Ha Noi",
                brandName: "Ashima",
                latitude: 10.123141,
                longitude: 11.21314,
                thumbnail: "https://....ggg.com.vn/path-to-ashima-trieu-viet-vuong-thumbnail.jpg",
                address: "Số 123 Triệu Việt Vương, Ba Đình, Hà Nội",
                avgRating: 4.56
            }
    }
```






