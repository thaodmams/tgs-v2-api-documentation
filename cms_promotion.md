# API Promotion news for the new TGS

## Promotion (post) object
- **id**: _int_ 
- **date**: _string_ e.g: "2018-05-08 16:11:12",
- **status**: _string_ trạng thái; "publish" - công khai, các trạng thái khác là ko hiển thị
- **title**: tiêu đề bài viết trong 
- **content**: nội dung bài viết 
- **excerpt**: nội dung tóm tắt
- **thumbnail**: _string_ url thumbnail
- **promotion_campaign**: _string_ promotion campaign
- **promotion_concept**: _string_ promotion concept
- **promotion_expired_date**: _string_ expired date      

## Category object
- **id**: _int_ 
- **description**: _string_ mô tả (nếu có)
- **name**: _string_ tên category
- **parent**: _string_ id danh mục cha; 0 là ko có cha

## 1. Get promotion
Version hiện tại cung cấp lấy promotion theo 4 category: Promotion (2), North(3), South (4), Middle (5).
Lấy promotion theo miền bắc, miền nam, miền trung.

### Method `GET`

### URI `/tgs-api-v1-1`

### Sending params
- **isHotPromotion**: _int_ (option) hot promotion. 1 - true; giá trị khác là false; Khi isHotPromotion tồn tại, tham số ProvinceId ko có tác dụng.
- **provinceId**: _int_ (option) province id từ CLM
- **concept**: _string_ (option) concept; Giá trị: hotspot/beer/western/bbq/others
- **thumbnailSize**: _string_ (option) kích thước ảnh đại diện; Giá trị: small/medium/large/full
- **promotionsPerPage**: _int_ (option) số lượng promotion mỗi page. Set -1 để lấy tất cả promotion
- **promotionPage**: _int_ (option) page cần lấy. Khi promotionsPerPage = -1, promotionPage ko có giá trị.

### Response
array post object

#### Example
- curl https://api...//tgs-api-v1-1/?provinceId=5&concept=Beer \ 
  -H 'Content-Type: application/json'
  -H 'Authorization: do stuff thing to authenticate'

- Response
```
    [
        {
          "id": 7,
          "date": "2018-05-08T16:11:12",
          "status": "publish",
          "type": "post",
          "title": "Mi\u1ec1n trung b\u00e0i s\u1ed1 2",
          "content": "<p>Thanks, that makes sense. khuy\u1ebfn m\u1ea1i t\u00ed n\u00e0o Where is this code supposed to go?<\/p>\n",
          "excerpt": "<p>Thanks, that makes sense. khuy\u1ebfn m\u1ea1i t\u00ed n\u00e0o Where is this code supposed to go?<\/p>\n",
          "thumbnail": "http://..../file.jpg",
          "promotion_campaign": ["GGCS"],
          "promotion_concept": ["beer","bbq"],
          "promotion_expired_date": ["2018-06-30"]
        },
        {
          "id": 5,
          "date": "2018-05-08T16:11:12",
          "status": "publish",
          "type": "post",
          "title": "Mi\u1ec1n trung b\u00e0i s\u1ed1 2",
          "content": "<p>Thanks, that makes sense. khuy\u1ebfn m\u1ea1i t\u00ed n\u00e0o Where is this code supposed to go?<\/p>\n",
          "excerpt": "<p>Thanks, that makes sense. khuy\u1ebfn m\u1ea1i t\u00ed n\u00e0o Where is this code supposed to go?<\/p>\n",
          "thumbnail": "http://..../file.jpg",
          "promotion_campaign": ["GGCS"],
          "promotion_concept": ["beer","bbq"],
          "promotion_expired_date": ["2018-06-30"]
        }
    ]
```
